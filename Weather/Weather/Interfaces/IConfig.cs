﻿namespace Weather.Interfaces
{
    using SQLite.Net.Interop;

    public interface IConfig
    {
        // Diretoria onde vai estar a base de dados onde se pode guardar
        string DirectoryDB { get; }

        // Propriedade que diz ao sqlLite qual é a plataforma que estamos a usaer
        ISQLitePlatform Platform { get; }

    }
}

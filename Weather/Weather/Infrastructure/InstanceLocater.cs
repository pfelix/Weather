﻿namespace Weather.Infrastructure
{
    using ViewModels;

    public class InstanceLocater
    {
        #region Properties

        public MainViewModel Main { get; set; }

        #endregion

        public InstanceLocater()
        {
            this.Main = new MainViewModel();
        }

    }
}

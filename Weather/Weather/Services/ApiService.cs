﻿namespace Weather.Services
{
    using Models.WebApi;
    using Newtonsoft.Json;
    using Plugin.Connectivity;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    public class ApiService
    {
        // Necessário Nuget Xam.Plugin.Connectivity
        /// <summary>
        /// Verifica a conexao a internet
        /// </summary>
        /// <returns></returns>
        public async Task<Response> CheckConnection()
        {
            try
            {
                // Verifica se existe ligação á internet
                if (!CrossConnectivity.Current.IsConnected)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = "Please see your Internet Config."
                    };
                }

                // Verifica se consegue aceder á internet
                var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");

                if (!isReachable)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = "Please see your Internet Connection."
                    };
                }

                return new Response
                {
                    IsSucess = true,
                    Message = "Internet Connection Ok."
                };

            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// Passa os dados da url, nome e password e recebe o token
        /// </summary>
        /// <param name="urlBase"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<TokenResponse> GetToken(string urlBase, string userName, string password)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                // Fazer o POST
                // StringContent - Criar a string e com o tipo de encriptação para enviar apela API
                var response = await client.PostAsync("Token",
                    new StringContent(string.Format(
                            "grant_type=password&username={0}&password={1}", userName, password),
                            Encoding.UTF8,
                            "application/x-www-form-urlencoded"));

                // Guardar resultado em Json
                var resultJson = await response.Content.ReadAsStringAsync();

                // Converter o resultado no tipo TokenResponse
                var result = JsonConvert.DeserializeObject<TokenResponse>(resultJson);

                return result;

            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// Recebe a url, o prefixo e o controlador e carrega os dados numa lista
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlBase"></param>
        /// <param name="servicePrefix"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        public async Task<Response> GetList<T>(string urlBase, string servicePrefix, string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var url = string.Format("{0}{1}", servicePrefix, controller);

                var response = await client.GetAsync(url);

                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result
                    };
                }

                var list = JsonConvert.DeserializeObject<List<T>>(result);

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }

        public async Task<Response> GetModel<T>(string urlBase, string servicePrefix, string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var url = string.Format("{0}{1}", servicePrefix, controller);

                var response = await client.GetAsync(url);

                var result = await response.Content.ReadAsStringAsync();

                var newResponse = JsonConvert.DeserializeObject<Response>(result);

                if (!response.IsSuccessStatusCode)
                {
                    newResponse.IsSucess = false;

                    return newResponse;
                }

                var list = JsonConvert.DeserializeObject<T>(result);

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }



        /// <summary>
        /// Metoto para fazer resgito de utilizador através de API
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlBase"></param>
        /// <param name="servicePrefix"></param>
        /// <param name="controller"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Response> ResgisterUser<T>(string urlBase, string servicePrefix, string controller, T model)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var url = string.Format("{0}{1}", servicePrefix, controller);

                // Serializar e converter em formato Json para enviar 
                var data = JsonConvert.SerializeObject(model);
                var content = new StringContent(data, Encoding.UTF8, "application/json");

                var response = await client.PostAsync(url, content);

                var resultJson = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<Response>(resultJson);

                if (!response.IsSuccessStatusCode)
                {
                    result.IsSucess = false;

                    return result;
                }

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok"
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// Metodo para receber lista de cidades do pais
        /// </summary>
        /// <param name="urlBase"></param>
        /// <param name="servicePrefix"></param>
        /// <param name="country"></param>
        /// <returns></returns>
        public async Task<Response> GetCities(string urlBase, string servicePrefix, string controller)
        {

            try
            {
                var client = new HttpClient();
                //client.BaseAddress = new Uri(urlBase);
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("X-Mashape-Key", "1MaPDSuedvmshdEa39fbXbvbyldFp17DOz5jsnpZCCnGrNWtZ5");

                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, $"{urlBase}{servicePrefix}{controller}");
                requestMessage.Headers.Add("X-Mashape-Key", "1MaPDSuedvmshdEa39fbXbvbyldFp17DOz5jsnpZCCnGrNWtZ5");

                HttpResponseMessage response = await client.SendAsync(requestMessage);

                var responseJson = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = responseJson
                    };
                }

                var list = JsonConvert.DeserializeObject<List<City>>(responseJson);

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }
    }
}

﻿namespace Weather.Models.Country
{
    using Newtonsoft.Json;

    public class Currency
    {
        [JsonProperty(PropertyName = "code")] // Isto é para poder usar c# e java, o java fica no JsonProperty, ou seja nome original
        public string Code { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "symbol")]
        public string Symbol { get; set; }
    }
}

﻿namespace Weather.Models.WebApi
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class ModelState
    {
        [JsonProperty(PropertyName = "")]
        public List<string> ErrorList { get; set; }
    }
}

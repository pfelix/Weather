﻿namespace Weather.Models.WebApi
{
    using System.Collections.Generic;

    public class Response
    {
        public bool IsSucess { get; set; }

        public string Message { get; set; }

        public object Result { get; set; }

        // Propriedade para retorno do Resgito de utilizadores é onde retorna os erros
        public ModelState ModelState { get; set; }
    }
}

﻿namespace Weather.Models.WebApi
{
    using Newtonsoft.Json;
    using SQLite.Net.Attributes;

    public  class City
    {
        [JsonProperty(PropertyName = "CityId")]
        [PrimaryKey]
        public int CityId { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "Lon")]
        public string Lon { get; set; }

        [JsonProperty(PropertyName = "Lat")]
        public string Lat { get; set; }

        public bool IsFavorite { get; set; }

        public override int GetHashCode()
        {
            return CityId;
        }
    }
}

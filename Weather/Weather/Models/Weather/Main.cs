﻿namespace Weather.Models.Weather
{
    using Newtonsoft.Json;

    public class Main
    {
        [JsonProperty(PropertyName = "temp")]
        public double Temp { get; set; }

        [JsonProperty(PropertyName = "pressure")]
        public double Pressure { get; set; }

        [JsonProperty(PropertyName = "humidity")]
        public int Humidity { get; set; }

        [JsonProperty(PropertyName = "temp_min")]
        public double TempMin { get; set; }

        [JsonProperty(PropertyName = "temp_max")]
        public double TempMax { get; set; }

        [JsonProperty(PropertyName = "sea_level")]
        public double SeaLevel { get; set; }

        [JsonProperty(PropertyName = "grnd_level")]
        public double GrndLevel { get; set; }
    }
}

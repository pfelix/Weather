﻿namespace Weather.Models.Weather
{
    using Newtonsoft.Json;

    public class Coord
    {
        [JsonProperty(PropertyName = "lon")]
        public double Lon { get; set; }

        [JsonProperty(PropertyName = "lat")]
        public double Lat { get; set; }
    }
}

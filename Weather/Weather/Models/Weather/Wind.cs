﻿namespace Weather.Models.Weather
{
    using Newtonsoft.Json;

    public class Wind
    {
        [JsonProperty(PropertyName = "speed")]
        public double Speed { get; set; }

        [JsonProperty(PropertyName = "deg")]
        public double Deg { get; set; }
    }
}

﻿namespace Weather.Models.Weather
{
    using Newtonsoft.Json;

    public class Clouds
    {
        [JsonProperty(PropertyName = "all")]
        public int All { get; set; }
    }

}

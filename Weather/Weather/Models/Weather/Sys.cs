﻿namespace Weather.Models.Weather
{
    using System;
    using Newtonsoft.Json;

    public class Sys
    {
        [JsonProperty(PropertyName = "type")]
        public int Type { get; set; }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "message")]
        public double Message { get; set; }

        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "sunrise")]
        public int Sunrise { get; set; }

        [JsonProperty(PropertyName = "sunset")]
        public int Sunset { get; set; }

        public DateTime SunriseDateTime
        {
            get
            {
                DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Sunrise);
                return dateTimeOffset.UtcDateTime;
            }
        }

        public DateTime SunsetDateTime
        {
            get
            {
                DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Sunset);
                return dateTimeOffset.UtcDateTime;
            }
        }
    }
}

﻿using Weather.Models.Weather;

namespace Weather.Helpers
{
    using Interfaces;
    using Models.WebApi;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using SQLite.Net;
    using SQLiteNetExtensions.Extensions;
    using Xamarin.Forms;

    /// <summary>
    /// Class principal de DB local
    /// </summary>
    public class DataAccess : IDisposable
    {
        private SQLiteConnection connection;

        public DataAccess()
        {
            // Vai buscar uma classe que implemenata o IConfig
            var config = DependencyService.Get<IConfig>();

            // Faz a ligação e Cria a BD se não existe
            this.connection = new SQLiteConnection(
                config.Platform,
                Path.Combine(config.DirectoryDB, "weather.db3"));

            // Cria uma tabela do tipo UserLocal
            connection.CreateTable<User>();
            connection.CreateTable<City>();

        }

        // Inserir
        public void Insert<T>(T model)
        {
            this.connection.Insert(model);
        }

        // Atualizar
        public void Update<T>(T model)
        {
            this.connection.Update(model);
        }

        // Apagar
        public void Delete<T>(T model)
        {
            this.connection.Delete(model);
        }

        // Ir buscar elementos numa lista
        public T First<T>(bool WithChildren) where T : class
        {
            if (WithChildren)
            {
                return connection.GetAllWithChildren<T>().FirstOrDefault();
            }
            else
            {
                return connection.Table<T>().FirstOrDefault();
            }
        }

        // Ir buscar uma lista
        public List<T> GetList<T>(bool WithChildren) where T : class
        {
            if (WithChildren)
            {
                return connection.GetAllWithChildren<T>().ToList();
            }
            else
            {
                return connection.Table<T>().ToList();
            }
        }

        // Procurar
        public T Find<T>(int pk, bool WithChildren) where T : class
        {
            if (WithChildren)
            {
                return connection.GetAllWithChildren<T>().FirstOrDefault(m => m.GetHashCode() == pk);
            }
            else
            {
                return connection.Table<T>().FirstOrDefault(m => m.GetHashCode() == pk);
            }
        }

        public void Dispose()
        {
            connection.Dispose();
        }
    }
}

﻿using Weather.Services;

namespace Weather.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models.Country;
    using Models.WebApi;
    using System.Collections.Generic;
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;

    public class MainViewModel
    {
        #region ViewModels

        public LoginViewModel Login { get; set; }

        public RegisterViewModel Register { get; set; }

        public CountriesViewModel Countries { get; set; }

        public CountryViewModel Country { get; set; }

        public CityViewModel City { get; set; }

        public MyFavoritesViewModel MyFavorites { get; set; }

        #endregion

        #region Properties

        public TokenResponse Token { get; set; }

        public List<Country> CountriesList { get; set; }

        public List<City> CitiesList { get; set; }

        #endregion

        #region Constructures

        public MainViewModel()
        {
            _instance = this;
            this.Login = new LoginViewModel();
        }

        #endregion

        #region Singleton - Design patterns

        private static MainViewModel _instance;

        public static MainViewModel GetInstance()
        {
            if (_instance == null)
            {
                return new MainViewModel();
            }

            return _instance;
        }

        #endregion

        #region Command

        //fazer o logout na toolbar.item
        public ICommand LogoutCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(Logout); }
        }

        public ICommand AboutCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(About); }
        }

        #endregion

        #region Methods

        private void Logout()
        {
            #region Apagar dados da table user (local) 

            DataServices dataServices = new DataServices();

            dataServices.DeleteAll<User>();
            dataServices.DeleteAll<City>();

            #endregion

            Application.Current.MainPage = new NavigationPage(new LoginPage());
        }

        private async void About()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new AboutPage());
        }

        #endregion


    }
}

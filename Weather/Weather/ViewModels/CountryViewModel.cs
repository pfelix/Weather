﻿namespace Weather.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models.Country;
    using Models.WebApi;
    using Services;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class CountryViewModel: BaseViewModel
    {
        #region Atributtes

        private ObservableCollection<CityItemViewModel> _citiesList;

        private bool _isRefreshing;

        private string _filter;

        private ApiService _apiService;

        #endregion

        #region Properties

        public Country Country { get; set; }

        public ObservableCollection<CityItemViewModel> CitiesList
        {
            get { return this._citiesList; }
            set { SetValue(ref this._citiesList, value); }
        }

        public bool IsRefreshing
        {
            get { return this._isRefreshing; }
            set { SetValue(ref this._isRefreshing, value); }
        }

        public string Filter
        {
            get { return this._filter; }
            set
            {
                SetValue(ref this._filter, value);
                this.Search();
            }
        }

        #endregion

        #region Constructor

        public CountryViewModel(Country country)
        {
            this.Country = country;
            this._apiService = new ApiService();
            this.LoadCities();
        }

        #endregion

        #region Commands

        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadCities); }
        }

        public ICommand SearchCommand
        {
            get { return new RelayCommand(Search); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Metodo para procurar por City
        /// </summary>
        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                // Temos que converter de Country para CountryViewModel por isso criamos um metodo
                this.CitiesList = new ObservableCollection<CityItemViewModel>(this.ToCityItemViewModel());
            }
            else
            {
                this.CitiesList = new ObservableCollection<CityItemViewModel>(
                    this.ToCityItemViewModel().Where(c => c.Name.ToLower().Contains(this.Filter.ToLower())
                    ));
            }
        }

        /// <summary>
        /// Metodo que vai converter de City para CityItemViewModel
        /// </summary>
        /// <returns></returns>
        private IEnumerable<CityItemViewModel> ToCityItemViewModel()
        {
            // Converte todos os dados da Lista<Country> para CountryItemViewModel
            //return this._countriesList.Select(c => new CountryItemViewModel
            return MainViewModel.GetInstance().CitiesList.Select(c => new CityItemViewModel
            {
                CityId = c.CityId,
                Name = c.Name,
                Country = c.Country,
                Lon = c.Lon,
                Lat = c.Lat
            }).OrderBy(c => c.Name);
        }

        /// <summary>
        /// Metodo para baixar as cidades dos pais da API
        /// </summary>
        private async void LoadCities()
        {
            IsRefreshing = true;

            // Testar a Conexão
            var connection = await this._apiService.CheckConnection();

            if (!connection.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Ok");

                IsRefreshing = false;

                return;
            }

            // Carrega da API
            var response = await this._apiService.GetList<City>("https://weatherwebapipf.azurewebsites.net", "/api", $"/Cities/GetCityByAlpha2Code?alpha2Code={Country.Alpha2Code}");

            if (!response.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    response.Message,
                    "Ok");

                IsRefreshing = false;

                return;
            }

            // Carregar a lista Para a MainViewModel
            MainViewModel.GetInstance().CitiesList = (List<City>)response.Result;

            // Carregar a lista da CountryViewModel
            this.CitiesList = new ObservableCollection<CityItemViewModel>(this.ToCityItemViewModel());


            IsRefreshing = false;
        }

        #endregion


    }
}

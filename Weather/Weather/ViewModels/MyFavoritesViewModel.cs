﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Weather.Models.WebApi;
using Weather.Services;
using Weather.Views;
using Xamarin.Forms;

namespace Weather.ViewModels
{
    public class MyFavoritesViewModel: BaseViewModel
    {
        #region Atributtes

        private DataServices _dataServices;

        private ObservableCollection<CityItemViewModel> _citiesList;

        private bool _isRefreshing;

        #endregion

        #region Properties

        public ObservableCollection<CityItemViewModel> CitiesList
        {
            get { return this._citiesList; }
            set { SetValue(ref this._citiesList, value); }
        }

        public bool IsRefreshing
        {
            get { return this._isRefreshing; }
            set { SetValue(ref this._isRefreshing, value); }
        }

        #endregion

        #region Constructor

        public MyFavoritesViewModel()
        {
            _dataServices = new DataServices();
            IsRefreshing = false;
            LoadCitiesLocal();
        }

        #endregion

        #region Commands

        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadCitiesLocal); }
        }

        public ICommand FindCityCommand
        {
            get { return new RelayCommand(FindCity); }
        }

        #endregion

        #region Methods

        private async void FindCity()
        {
            MainViewModel.GetInstance().Countries = new CountriesViewModel();

            await Application.Current.MainPage.Navigation.PushAsync(new CountriesPage());
        }

        /// <summary>
        /// Metodo para carregar as cidades que estão na bd local que são os favoritos
        /// </summary>
        public void LoadCitiesLocal()
        {
            IsRefreshing = true;


            List<City> localCities = _dataServices.Get<City>(false);

            // Carregar a lista Para a MainViewModel
            MainViewModel.GetInstance().CitiesList = localCities;

            // Carregar a lista da CountryViewModel
            this.CitiesList = new ObservableCollection<CityItemViewModel>(this.ToCityItemViewModel());

            IsRefreshing = false;
        }

        /// <summary>
        /// Metodo que vai converter de City para CityItemViewModel
        /// </summary>
        /// <returns></returns>
        private IEnumerable<CityItemViewModel> ToCityItemViewModel()
        {
            // Converte todos os dados da Lista<Country> para CountryItemViewModel
            //return this._countriesList.Select(c => new CountryItemViewModel
            return MainViewModel.GetInstance().CitiesList.Select(c => new CityItemViewModel
            {
                CityId = c.CityId,
                Name = c.Name,
                Country = c.Country,
                Lon = c.Lon,
                Lat = c.Lat
            }).OrderBy(c => c.Name);
        }

        #endregion



    }
}

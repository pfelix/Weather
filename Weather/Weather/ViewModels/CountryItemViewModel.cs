﻿namespace Weather.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Views;
    using System.Windows.Input;
    using Xamarin.Forms;
    using Models.Country;

    /// <summary>
    /// Classe para consegue fazer o clik no pais, para conseguir utilizar a opção do Commando
    /// </summary>
    public class CountryItemViewModel : Country
    {
        public ICommand SelectCountryCommand
        {
            get { return new RelayCommand(SelectCountry); }
        }

        private async void SelectCountry()
        {
            MainViewModel.GetInstance().Country = new CountryViewModel(this);

            await Application.Current.MainPage.Navigation.PushAsync(new CountryPage());
        }
    }
}

﻿using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace Weather.ViewModels
{
    using Models.Weather;
    using Services;
    using System;
    using Models.WebApi;
    using Xamarin.Forms;

    public class CityViewModel: BaseViewModel
    {
        #region Atributtes

        private ApiService _apiService;

        private DataServices _dataServices;

        private CityWeather _cityWeather;

        private bool _isRunnig;

        private string _image;

        private bool _isFavorites;

        private string _isFavoriteImage;


        #endregion

        #region Properties

        public City City { get; set; }

        public string IsFavoriteImage 
        {
            get { return _isFavoriteImage; }
            set { SetValue(ref this._isFavoriteImage, value); }
        }

        public bool IsFavorites
        {
            get { return _isFavorites; }
            set
            {
                SetValue(ref this._isFavorites, value);
            }
        }

        public CityWeather CityWeather
        {
            get { return _cityWeather; }
            set { SetValue(ref this._cityWeather, value);}
        }

        public bool IsRunnig
        {
            get { return _isRunnig; }
            set { SetValue(ref this._isRunnig, value);}
        }

        public string Image
        {
            get { return _image; }
            set { SetValue(ref this._image, value); }
        }

        #endregion

        #region Constructor

        public CityViewModel(City city)
        {
            this._apiService = new ApiService();
            this._dataServices = new DataServices();
            this.City = city;
            this.IsRunnig = false;
            LoadCityWeather();
        }

        #endregion

        #region Commands

        public ICommand IsFavoriteCommand
        {
            get { return new RelayCommand(IsFavorite); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Metodo para baixar a tempo da cidade da API
        /// </summary>
        private async void LoadCityWeather()
        {
            IsRunnig = true;

            // Testar a Conexão
            var connection = await this._apiService.CheckConnection();

            if (!connection.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Ok");

                // Volta á página anterior
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            CountryViewModel country = MainViewModel.GetInstance().Country;

            // Carrega da API
            var response = await this._apiService.GetModel<CityWeather>("https://api.openweathermap.org", "/data", $"/2.5/weather?id={City.CityId}&units=metric&APPID=b2465087bd7433f8cb48ef76f469799f");

            if (!response.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    response.Message,
                    "Ok");

                return;
            }

            // Carregar a lista Para a MainViewModel
            CityWeather = (CityWeather)response.Result;

            // Verificar se cidade é favorito para mostar a respetiva imagem
            var findCityLocal = _dataServices.Find<City>(CityWeather.Id, false);

            if (findCityLocal != null)
            {
                this.IsFavorites = true;
                this.IsFavoriteImage = "ic_estrela_true";
            }
            else
            {
                this.IsFavorites = false;
                this.IsFavoriteImage = "ic_estrela_false";
            }

            this.Image = "ic_" + CityWeather.Weathers[0].Icon + ".jpg";

            IsRunnig = false;
        }

        /// <summary>
        /// Metodo que altera de true para false consuante o valor anterior e guarda na BD local
        /// </summary>
        private async void IsFavorite()
        {
            bool isRemembered = MainViewModel.GetInstance().Login.IsRemembered;

            if (!isRemembered)
            {
                await Application.Current.MainPage.DisplayAlert("Warnig", "My Favorites will not work, because in the login did not check the option to remember.", "Ok");

                return;
            }


            City cityToInsertOrDelete = new City
            {
                CityId = this.City.CityId,
                Name = this.City.Name,
                Country = this.City.Country,
                Lon = this.City.Lon,
                Lat = this.City.Lat,
            };

            if (IsFavorites)
            {
                IsFavorites = false;

                IsFavoriteImage = "ic_estrela_false";

                cityToInsertOrDelete.IsFavorite = IsFavorites;

                _dataServices.Delete(cityToInsertOrDelete);
            }
            else
            {
                IsFavorites = true;

                IsFavoriteImage = "ic_estrela_true";

                cityToInsertOrDelete.IsFavorite = IsFavorites;

                _dataServices.InsertOrUpdate(cityToInsertOrDelete);
            }
        }

        #endregion
    }
}

﻿namespace Weather.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models.Country;
    using Services;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class CountriesViewModel: BaseViewModel
    {
        #region Atributtes

        private ObservableCollection<CountryItemViewModel> _countries;

        private bool _isRefreshing;

        private string _filter;

        private ApiService _apiService;

        #endregion

        #region Properties

        public ObservableCollection<CountryItemViewModel> CountriesList
        {
            get { return this._countries; }
            set { SetValue(ref this._countries, value); }
        }

        public bool IsRefreshing
        {
            get { return this._isRefreshing; }
            set { SetValue(ref this._isRefreshing, value); }
        }

        public string Filter
        {
            get { return this._filter; }
            set
            {
                SetValue(ref this._filter, value);
                this.Search();
            }
        }

        #endregion

        #region Constructor

        public CountriesViewModel()
        {
            this._apiService = new ApiService();
            this.LoadCountries();
        }

        #endregion

        #region Commands

        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadCountries); }
        }

        public ICommand SearchCommand
        {
            get { return new RelayCommand(Search); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Metodo para procurar por Country
        /// </summary>
        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                // Temos que converter de Country para CountryViewModel por isso criamos um metodo
                this.CountriesList = new ObservableCollection<CountryItemViewModel>(this.ToCountryItemViewModel());
            }
            else
            {
                this.CountriesList = new ObservableCollection<CountryItemViewModel>(
                    this.ToCountryItemViewModel().Where(c => c.Name.ToLower().Contains(this.Filter.ToLower()) ||
                                                             c.Capital.ToLower().Contains(this.Filter.ToLower())
                    ));
            }
        }

        /// <summary>
        /// Metodo que vai converter de Country para CountryItemViewModel
        /// </summary>
        /// <returns></returns>
        private IEnumerable<CountryItemViewModel> ToCountryItemViewModel()
        {
            // Converte todos os dados da Lista<Country> para CountryItemViewModel
            //return this._countriesList.Select(c => new CountryItemViewModel
            return MainViewModel.GetInstance().CountriesList.Select(c => new CountryItemViewModel
            {
                Alpha2Code = c.Alpha2Code,
                Alpha3Code = c.Alpha3Code,
                AltSpellings = c.AltSpellings,
                Area = c.Area,
                Borders = c.Borders,
                CallingCodes = c.CallingCodes,
                Capital = c.Capital,
                Cioc = c.Cioc,
                Currencies = c.Currencies,
                Demonym = c.Demonym,
                Flag = c.Flag,
                Gini = c.Gini,
                Languages = c.Languages,
                Latlng = c.Latlng,
                Name = c.Name,
                NativeName = c.NativeName,
                NumericCode = c.NumericCode,
                Population = c.Population,
                Region = c.Region,
                RegionalBlocs = c.RegionalBlocs,
                Subregion = c.Subregion,
                Timezones = c.Timezones,
                TopLevelDomain = c.TopLevelDomain,
                Translations = c.Translations
            }).OrderBy(c => c.Name);
        }

        /// <summary>
        /// Metodo para baixar informação dos paises de API
        /// </summary>
        private async void LoadCountries()
        {
            IsRefreshing = true;

            // Testar a Conexão
            var connection = await this._apiService.CheckConnection();

            if (!connection.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Ok");

                IsRefreshing = false;

                return;
            }

            // Carrega da API
            var response = await this._apiService.GetList<Country>("http://restcountries.eu", "/rest", "/v2/all");

            if (!response.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Erro",
                    response.Message,
                    "Ok");

                IsRefreshing = false;

                return;
            }

            // Carregar a lista
            MainViewModel.GetInstance().CountriesList = (List<Country>)response.Result;

            // A listeview (lado cliente) só consegu-se ligar a uma ObservableCollection
            this.CountriesList = new ObservableCollection<CountryItemViewModel>(this.ToCountryItemViewModel());

            IsRefreshing = false;
        }

        #endregion


    }
}

﻿namespace Weather.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models.WebApi;
    using Services;
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;

    public class LoginViewModel : BaseViewModel
    {
        #region Attributes

        private string _email;

        private string _password;

        private bool _isEnabled;

        private bool _isRunnig;

        private bool _isRemembered;

        private ApiService _apiService;

        private DataServices _dataServices;

        #endregion

        #region Properties

        public bool IsRemembered
        {
            get { return _isRemembered; }
            set { SetValue(ref this._isRemembered, value); }
        }

        public string Email
        {
            get { return _email; }
            set { SetValue(ref this._email, value); }
        }

        public string Password
        {
            get { return _password; }
            set { SetValue(ref this._password, value); }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { SetValue(ref this._isEnabled, value); }
        }

        public bool IsRunnig
        {
            get { return _isRunnig; }
            set { SetValue(ref this._isRunnig, value); }
        }

        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(Login); }
        }

        public ICommand RegisterCommand
        {
            get
            {
                return new RelayCommand(Register);
            }
        }

        #endregion

        #region Constructores

        public LoginViewModel()
        {
            //this.Email = "paulo.afg85@gmail.com";
            //this.Password = "P@ssw0rd";
            this._apiService = new ApiService();
            this._dataServices = new DataServices();
            this.IsEnabled = true;
            this.IsRunnig = false;
            this.IsRemembered = true;

            LoadUserLocal();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Metodo para fazer login e guardar localmente user e senha se marcado remember
        /// </summary>
        private async void Login()
        {
            #region Verifircar se Email e Senha estão preenchidos

            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter email, please.",
                    "Ok");
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter password, please.",
                    "Ok");
                return;
            }

            #endregion

            this.IsRunnig = true;
            this.IsEnabled = false;

            //Autenticação usando o Token (API)

            #region Testar conecção

            var connection = await this._apiService.CheckConnection();

            if (!connection.IsSucess)
            {
                #region Verificar DB Local

                User userLocal = GetUserLocal();

                if (userLocal != null)
                {
                    if (!(userLocal.Email == Email && userLocal.Password == Password))
                    {
                        this.IsRunnig = false;
                        this.IsEnabled = true;
                        await Application.Current.MainPage.DisplayAlert("Error", connection.Message, "Ok");
                        return;
                    }
                }
                else
                {
                    this.IsRunnig = false;
                    this.IsEnabled = true;
                    await Application.Current.MainPage.DisplayAlert("Error", connection.Message, "Ok");
                    return;
                }

                #endregion
            }
            else
            {
                #region Get Token

                var token = await this._apiService.GetToken("https://weatherwebapipf.azurewebsites.net/", this.Email,
                    this.Password);

                if (token == null)
                {
                    this.IsRunnig = false;
                    this.IsEnabled = true;
                    await Application.Current.MainPage.DisplayAlert("Error", "Something was wrong, please try later.", "Ok");
                    return;
                }

                // Token vem vazio (Exp. login inválido)
                if (string.IsNullOrEmpty(token.AccessToken))
                {
                    this.IsRunnig = false;
                    this.IsEnabled = true;
                    await Application.Current.MainPage.DisplayAlert("Error", token.ErrorDescription, "Ok");
                    return;
                }

                // Atribuir à propriedade que está no MainViewModel o token para ser acedido no resto da aplicação
                MainViewModel.GetInstance().Token = token;

                #endregion
            }

            #endregion

            #region Verificar se Login é o mesmo que está na bd

            User user = _dataServices.First<User>(false);

            if (user == null)
            {
                _dataServices.DeleteAll<User>();
                _dataServices.DeleteAll<City>();
            }
            else
            {
                if (user.Email != this.Email)
                {
                    _dataServices.DeleteAll<User>();
                    _dataServices.DeleteAll<City>();
                }
            }

            #endregion

            #region Verificar se está marcado opção de remember

            if (IsRemembered)
            {
                User userLocal = new User
                {
                    UserId = 1,
                    Email = this.Email,
                    Password = this.Password,
                };

                _dataServices.InsertOrUpdate(userLocal);
            }
            else
            {
                _dataServices.DeleteAll<User>();
            }

            #endregion

            this.IsRunnig = false;
            this.IsEnabled = true;

            this.Email = string.Empty;
            this.Password = string.Empty;

            // Instanciar o CountriesViesModel
            //MainViewModel.GetInstance().Countries = new CountriesViewModel();
            MainViewModel.GetInstance().MyFavorites = new MyFavoritesViewModel();


            // Torna a ContinentsPage como MainPage
            //Application.Current.MainPage = new NavigationPage(new CountriesPage());
            Application.Current.MainPage = new NavigationPage(new MyFavoritesPage());
        }

        /// <summary>
        /// Apagar user da DB local
        /// </summary>
        /// <returns></returns>
        private User GetUserLocal()
        {
            User userLocal = _dataServices.First<User>(false);

            // Verifica se existe algum login guardado na BD
            if (userLocal == null)
            {
                return null;
            }

            return userLocal;
        }

        /// <summary>
        /// Avança para a página Register
        /// </summary>
        private void Register()
        {
            MainViewModel.GetInstance().Register = new RegisterViewModel();
            Application.Current.MainPage.Navigation.PushAsync(new RegisterPage());
        }

        /// <summary>
        /// Lê da BD local o utilizador
        /// </summary>
        private void LoadUserLocal()
        {
            try
            {
                User userLocal = _dataServices.First<User>(false);

                if (userLocal.Password != null)
                {
                    this.Email = userLocal.Email;
                    this.Password = userLocal.Password;
                }
            }
            catch
            {

            }

        }

        #endregion

    }
}

﻿namespace Weather.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    // Para usar o delegate primeiro fazer herança do INotifyPropertyChanged
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        // [CallerMemberName] - Para conseguir buscar o nome da propriedade e a própria propriedade
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(propertyName));
        }

        // T - quer dizer que o tipo é genérico, aceita tudo
        // Metodo que alterar o set
        protected void SetValue<T>(ref T backingField, T value, [CallerMemberName] string propertyName = null)
        {
            // Verifica se os campos são do mesmo tipo EqualityComparer<T>.Default
            // Depois verifica se foram alterados Equals(backingField, value)
            // No entanto o que é verificado é se foi alterado, metemos o EqualityComparer<T> porque é obrigatório para fazer desta forma
            if (EqualityComparer<T>.Default.Equals(backingField, value))
            {
                return;
            }

            // Altera o campo com o novo dado
            backingField = value;

            // Chama-se o metodo para fazer o delegate
            OnPropertyChanged(propertyName);
        }
    }
}

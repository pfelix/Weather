﻿namespace Weather.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models.WebApi;
    using Services;
    using System;
    using System.Net.Mail;
    using System.Windows.Input;
    using Xamarin.Forms;


    public class RegisterViewModel : BaseViewModel
    {
        #region Attributes

        private string _email;

        private string _password;

        private string _passwordConfirm;

        private bool _isRunnig;

        private bool _isEnabled;

        private readonly ApiService _apiService;

        #endregion

        #region Properties

        public string Email
        {
            get { return _email; }
            set { SetValue(ref this._email, value); }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                SetValue(ref this._password, value);
                this.ComparePassword();
            }
        }

        public string PasswordConfirm
        {
            get { return _passwordConfirm; }
            set
            {
                SetValue(ref this._passwordConfirm, value);
                this.ComparePassword();
            }
        }

        public bool IsRunnig
        {
            get { return _isRunnig; }
            set { SetValue(ref this._isRunnig, value); }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { SetValue(ref this._isEnabled, value); }
        }

        #endregion

        #region Commands

        public ICommand RegisterCommand
        {
            get { return new RelayCommand(Register); }
        }

        private async void Register()
        {
            #region Validation

            // Verificar se Email preenchido
            if (string.IsNullOrEmpty(Email))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "You need to indicate an email.", "Ok");
                return;
            }

            // Verificar se Password preechida
            if (string.IsNullOrEmpty(Password))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "You need to indicate a email.", "Ok");
                return;
            }

            // Verificar se PasswordConfirm preenchido
            if (string.IsNullOrEmpty(PasswordConfirm))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "You need to confirm the password.", "Ok");
                return;
            }

            // Verificar se é um email
            if (!IsValidEmail(Email))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "You need to indicate a valid email address.", "Ok");
                return;
            }

            // Verifica se Password com 6 carateres
            if (PasswordLenght())
            {
                await Application.Current.MainPage.DisplayAlert("Error", "The password must be at least 6 characters.", "Ok");
                return;
            }

            #endregion
            
            this.IsRunnig = true;
            this.IsEnabled = false;
            
            #region Testar conecção

            var connection = await this._apiService.CheckConnection();

            if (!connection.IsSucess)
            {
                this.IsRunnig = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert("Error", connection.Message, "Ok");
                return;
            }

            #endregion

            #region Fazer registo

            // Criar objeto para enviar dados do tipo user
            User user = new User
            {
                Email = this.Email,
                Password = this.Password,
                ConfirmPassword = this.PasswordConfirm
            };

            var register = await this._apiService.ResgisterUser("https://weatherwebapipf.azurewebsites.net/","api/","Account/Register", user);


            if (!register.IsSucess)
            {
                // Mostrar os erros que retornaram da API
                string error = String.Empty;

                foreach (var errorItem in register.ModelState.ErrorList)
                {
                    // Para não duplicar a informação do email
                    if (!errorItem.StartsWith("Name"))
                    {
                        error += errorItem + Environment.NewLine;
                    }
                }
                
                this.IsRunnig = false;
                this.IsEnabled = true;

                await Application.Current.MainPage.DisplayAlert("Error", error, "Ok");
                return;
            }

            await Application.Current.MainPage.DisplayAlert("Resgister", "Registration successful", "Ok");

            this.IsRunnig = false;
            this.IsEnabled = true;

            // Foltar para a página anterior (Login)
            await Application.Current.MainPage.Navigation.PopAsync();

            #endregion

        }

        #endregion

        #region Constructores

        public RegisterViewModel()
        {
            this.IsEnabled = false;
            this.IsRunnig = false;
            this._apiService = new ApiService();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Verifica se password é igual
        /// </summary>
        private void ComparePassword()
        {
            if (this.Password == PasswordConfirm)
            {
                this.IsEnabled = true;
            }
            else
            {
                this.IsEnabled = false;
            }
        }

        /// <summary>
        /// Verifica se password tem no minimo 6 carates. Retorna verdade se for menos que 6
        /// </summary>
        private bool PasswordLenght()
        {
            if (this.Password.Length < 6)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Verificar se é um email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private bool IsValidEmail(string email)
        {
            try
            {
                MailAddress mail = new MailAddress(email);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        #endregion


    }
}

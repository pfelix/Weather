﻿namespace Weather.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models.WebApi;
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;

    public class CityItemViewModel: City
    {
        public ICommand SelectCityCommand
        {
            get { return new RelayCommand(SelectCity); }
        }

        private async void SelectCity()
        {
            MainViewModel.GetInstance().City = new CityViewModel(this);

            await Application.Current.MainPage.Navigation.PushAsync(new CityTabbedPage());
        }
    }
}

﻿using Weather.Models.WebApi;
using Weather.Services;

namespace Weather
{
    using Views;
    using Xamarin.Forms;

    public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

		    //MainPage = new LoginPage();
		    MainPage = new NavigationPage(new LoginPage());
        }

		protected override void OnStart ()
		{
		    #region Verificar DB Local

		    User userLocal = GetUserLocal();

		    if (userLocal == null)
		    {
		        MainPage = new NavigationPage(new LoginPage());
		        return;
		    }

		    MainPage = new NavigationPage(new MyFavoritesPage());

		    #endregion
        }

        protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
		    #region Verificar DB Local

		    User userLocal = GetUserLocal();

		    if (userLocal == null)
		    {
		        MainPage = new NavigationPage(new LoginPage());
                return;
            }

		    MainPage = new NavigationPage(new MyFavoritesPage());
            
		    #endregion

        }

	    /// <summary>
	    /// Apagar user da DB local
	    /// </summary>
	    /// <returns></returns>
	    private User GetUserLocal()
	    {
            DataServices _dataServices = new DataServices();

	        User userLocal = _dataServices.First<User>(false);

	        // Verifica se existe algum login guardado na BD
	        if (userLocal == null)
	        {
	            return null;
	        }

	        return userLocal;
	    }
    }
}

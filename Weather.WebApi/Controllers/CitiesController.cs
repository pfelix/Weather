﻿namespace Weather.WebApi.Controllers
{
    using Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    public class CitiesController : ApiController
    {
        private WeatherWebApiContext db = new WeatherWebApiContext();
        /// <summary>
        /// Oneter uma lista de cidades filtrado pelo Alph2Code
        /// </summary>
        /// <param name="alpha2Code"></param>
        /// <returns></returns>
        // GET: api/Cities/5
        [Route("api/Cities/GetCityByAlpha2Code")]
        public IHttpActionResult GetCityByAlpha2Code(string alpha2Code)
        {
            List<City> city = db.Cities.Where(c => c.Country == alpha2Code).ToList();

            if (city.Count == 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "We\'re sorry but we do not have any cities in this country."));
            }

            return Ok(city);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
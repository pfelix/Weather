﻿namespace Weather.WebApi.Models
{
    using System.ComponentModel.DataAnnotations;

    public class City
    {
        [Key]
        public int CityId { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public double Lon { get; set; }

        public double Lat { get; set; } 

    }
}